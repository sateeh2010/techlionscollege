<?php

require 'vendor/autoload.php';
include_once('check_login_status.php');
$loader = new Twig_Loader_Filesystem("views");
$twig = new Twig_Environment($loader);
function forget_password_render($message,$error){
		global $dbcon,$user_data,$twig;
		$dbcon->close();
		echo $twig->render('forget_password.html',array('message'=>$message,'error'=>$error));
		

}
//Remove a course from the data base. 
function forget_password(){
	global $dbcon,$user_data,$user_ok;
		$email = $_POST['email'];
		$stmt = $dbcon->prepare("SELECT * FROM PERSON WHERE EMAIL = ?  LIMIT 1");
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_array();
		if($result){
		
		
		$mailbody = "Dear user, ".$result['FNAME']." If this e-mail does not apply to you please ignore it. It appears that you have forgetten  your password. Your user name is your email address, and your password is : ".$result['PASSWD']."";
		mail($email, "www.techlionscollege.appspot.com - Password Reset", $mailbody);
		forget_password_render('Your password has been sent to your email, please check your email and log in',false);
		}
		else{
			forget_password_render('Please enter your email again',true);
		}
		
		

}
if ($_SERVER['REQUEST_METHOD'] === 'POST'){

	if(isset($_POST['forget_password']))
	{
		forget_password();
	}
}

else{
	forget_password_render(null,false);

	}	


?>